class BusRepository {
  final int id;
  final String route;
  final int departureHour;
  final int departureMinute;
  final int arrivalHour;
  final int arrivalMinute;

  BusRepository({
    required this.id,
    required this.route,
    required this.departureHour,
    required this.departureMinute,
    required this.arrivalHour,
    required this.arrivalMinute,
  });
}
