import 'package:flutter/material.dart';
import 'package:bus_app/models/model.dart';
import 'package:bus_app/repository/repository.dart';
import 'package:bus_app/pages/details.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final DatabaseProvider _databaseProvider = DatabaseProvider();

  String? selectedRoute;
  String? returnRoute;
  List<String> routeCombinations = [];
  List<BusRepository> schedules = [];
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _loadRouteCombinations();
  }

  Future<void> _loadRouteCombinations() async {
    final loadedRouteCombinations =
        await _databaseProvider.getAllRouteCombinations();
    setState(() {
      routeCombinations = loadedRouteCombinations;
    });
  }

  void searchSchedules() async {
    if (selectedRoute != null) {
      setState(() {
        isLoading = true;
      });
      final results =
          await _databaseProvider.getSchedulesForRoute(selectedRoute!);
      final returnResults =
          await _databaseProvider.generateReturnRoute(selectedRoute!);
      setState(() {
        schedules = results;
        returnRoute = returnResults;
        isLoading = false;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Selecione uma rota')),
      );
    }
  }

  void clearSearch() {
    setState(() {
      selectedRoute = null;
      returnRoute = null;
      schedules = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Horários dos Ônibus'),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: searchSchedules,
            tooltip: 'Recarregar Horarios',
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            DropdownButtonFormField<String>(
              decoration: const InputDecoration(
                labelText: 'Selecione a rota',
                border: OutlineInputBorder(),
              ),
              value: selectedRoute,
              items: routeCombinations.map((route) {
                return DropdownMenuItem<String>(
                  value: route,
                  child: Text(route),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  selectedRoute = value;
                });
              },
            ),
            const SizedBox(height: 20),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton.icon(
                    onPressed: searchSchedules,
                    icon: const Icon(Icons.search),
                    label: const Text('Pesquisar'),
                  ),
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: ElevatedButton.icon(
                    onPressed: clearSearch,
                    icon: const Icon(Icons.clear),
                    label: const Text('Limpar'),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.redAccent,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20),
            Expanded(
              child: isLoading
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : schedules.isEmpty
                      ? const Center(
                          child: Text(
                            'Não há horários disponíveis.',
                            style: TextStyle(fontSize: 18, color: Colors.grey),
                          ),
                        )
                      : ListView.builder(
                          itemCount: schedules.length,
                          itemBuilder: (context, index) {
                            final schedule = schedules[index];

                            int returnHour = schedule.arrivalHour;
                            int returnMinute = schedule.arrivalMinute + 10;

                            if (returnMinute >= 60) {
                              returnMinute -= 60;
                              returnHour = (returnHour + 1) % 24;
                            }

                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 4.0),
                              child: Card(
                                elevation: 4,
                                child: ListTile(
                                  leading: const Icon(
                                    Icons.directions_bus,
                                    color: Colors.blueAccent,
                                  ),
                                  title: Text(
                                    'Rota: ${schedule.route}',
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Horário de partida: ${schedule.departureHour.toString().padLeft(2, '0')}:${schedule.departureMinute.toString().padLeft(2, '0')}',
                                        style: const TextStyle(
                                            color: Colors.black54),
                                      ),
                                      Text(
                                        'Horário de chegada: ${schedule.arrivalHour.toString().padLeft(2, '0')}:${schedule.arrivalMinute.toString().padLeft(2, '0')}',
                                        style: const TextStyle(
                                            color: Colors.black54),
                                      ),
                                    ],
                                  ),
                                  trailing:
                                      const Icon(Icons.arrow_forward_ios, size: 16),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => DetailsPage(
                                          schedule: schedule,
                                          returnRoute: returnRoute!,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            );
                          },
                        ),
            ),
          ],
        ),
      ),
    );
  }
}
