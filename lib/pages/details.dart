import 'package:flutter/material.dart';
import 'package:bus_app/repository/repository.dart';

class DetailsPage extends StatelessWidget {
  final BusRepository schedule;
  final String returnRoute;

  const DetailsPage({super.key, required this.schedule, required this.returnRoute});

  @override
  Widget build(BuildContext context) {
    int returnHour = schedule.arrivalHour;
    int returnMinute = schedule.arrivalMinute + 10;

    if (returnMinute >= 60) {
      returnMinute -= 60;
      returnHour = (returnHour + 1) % 24;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Detalhes do Ônibus'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Rota: ${schedule.route}',
              style: const TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(height: 16),
            const Divider(),
            const SizedBox(height: 16),
            const Text(
              'Previsão estimada. Pode sofrer alterações devido ao trânsito.',
              style: TextStyle(
                fontSize: 16,
                fontStyle: FontStyle.italic,
                color: Colors.grey,
              ),
            ),
            const SizedBox(height: 24),
            Row(
              children: [
                const Icon(Icons.directions_bus, color: Colors.blueAccent),
                const SizedBox(width: 8),
                const Text(
                  'Horário de saída:',
                  style: TextStyle(fontSize: 20, color: Colors.black54),
                ),
                Spacer(),
                Text(
                  '${schedule.departureHour.toString().padLeft(2, '0')}:${schedule.departureMinute.toString().padLeft(2, '0')}',
                  style: const TextStyle(fontSize: 20, color: Colors.black87),
                ),
              ],
            ),
            const SizedBox(height: 12),
            Row(
              children: [
                const Icon(Icons.location_on, color: Colors.redAccent),
                const SizedBox(width: 8),
                const Text(
                  'Horário de chegada:',
                  style: TextStyle(fontSize: 20, color: Colors.black54),
                ),
                Spacer(),
                Text(
                  '${schedule.arrivalHour.toString().padLeft(2, '0')}:${schedule.arrivalMinute.toString().padLeft(2, '0')}',
                  style: const TextStyle(fontSize: 20, color: Colors.black87),
                ),
              ],
            ),
            const SizedBox(height: 24),
            const Divider(),
            const SizedBox(height: 24),
            Text(
              'Retorno: $returnRoute',
              style: const TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(height: 12),
            Row(
              children: [
                const Icon(Icons.access_time, color: Colors.greenAccent),
                const SizedBox(width: 8),
                const Text(
                  'Horário de retorno:',
                  style: TextStyle(fontSize: 20, color: Colors.black54),
                ),
                const Spacer(),
                Text(
                  '${returnHour.toString().padLeft(2, '0')}:${returnMinute.toString().padLeft(2, '0')}',
                  style: const TextStyle(fontSize: 20, color: Colors.black87),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
