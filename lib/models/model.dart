import 'dart:async';
import 'dart:math';
import 'package:bus_app/repository/repository.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

const String routeTable = "route";
const String scheduleTable = "schedule";
const String idColumn = "id";
const String routeIdColumn = "route_id";
const String routeColumn = "route";
const String hourColumn = "hour";
const String minuteColumn = "minute";
const String arrivalHourColumn = "arrival_hour";
const String arrivalMinuteColumn = "arrival_minute";

class DatabaseProvider {
  static final DatabaseProvider _instance = DatabaseProvider._internal();

  factory DatabaseProvider() => _instance;

  DatabaseProvider._internal();
  Database? _db;

  Future<Database> get db async {
    if (_db != null) return _db!;
    _db = await _initDb();
    return _db!;
  }

  Future<Database> _initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "bus.db");

    return await openDatabase(
      path,
      version: 1,
      onCreate: (db, version) async {
        await db.execute('''
          CREATE TABLE $routeTable (
            $idColumn INTEGER PRIMARY KEY,
            $routeColumn TEXT
          )
        ''');

        await db.execute('''
          CREATE TABLE $scheduleTable (
            $idColumn INTEGER PRIMARY KEY,
            $routeIdColumn INTEGER,
            $hourColumn INTEGER,
            $minuteColumn INTEGER,
            $arrivalHourColumn INTEGER,
            $arrivalMinuteColumn INTEGER
          )
        ''');

        await _addInitialData(db);
      },
    );
  }

  Future<void> _addInitialData(Database db) async {
    final routes = [
      'Centro - Vila São Francisco',
      'Vila São Francisco - Vila Operária',
      'Vila Operária - Vila Industrial',
      'Vila Industrial - Jardim Panorama',
      'Jardim Panorama - Vila Pioneiro',
      'Vila Pioneiro - Vila Concórdia',
      'Vila Concórdia - Jardim La Salle',
      'Jardim La Salle - Jardim Bressan',
      'Jardim Bressan - Vila Becker',
      'Vila Becker - Vila Boa Esperança',
    ];

    Batch batch = db.batch();

    for (final route in routes) {
      batch.insert(routeTable, {routeColumn: route});
    }

    await batch.commit(noResult: true);

    final random = Random();
    final List<Map<String, dynamic>> routeRecords = await db.query(routeTable);

    batch = db.batch();

    for (final routeRecord in routeRecords) {
      final routeId = routeRecord[idColumn];
      for (int i = 0; i < 40; i++) {
        final hour = random.nextInt(18) + 6;
        final minute = random.nextInt(60);

        if (hour == 23 && minute > 30) {
          continue;
        }

        final arrivalHour = (hour + 1) % 24;
        final arrivalMinute = minute;

        batch.insert(
          scheduleTable,
          {
            routeIdColumn: routeId,
            hourColumn: hour,
            minuteColumn: minute,
            arrivalHourColumn: arrivalHour,
            arrivalMinuteColumn: arrivalMinute,
          },
        );
      }
    }

    await batch.commit(noResult: true);
  }

  Future<List<BusRepository>> getAllSchedules() async {
    final db = await this.db;
    final List<Map<String, dynamic>> maps = await db.query(scheduleTable);

    return maps.map((map) {
      return BusRepository(
        id: map[idColumn],
        route: map[routeColumn],
        departureHour: map[hourColumn],
        departureMinute: map[minuteColumn],
        arrivalHour: map[arrivalHourColumn],
        arrivalMinute: map[arrivalMinuteColumn],
      );
    }).toList();
  }

  Future<List<String>> getAllRouteCombinations() async {
    final db = await this.db;
    final List<Map<String, dynamic>> maps = await db.query(routeTable);

    return maps.map((map) => map[routeColumn] as String).toList();
  }

  Future<List<BusRepository>> getSchedulesForRoute(String route) async {
    final db = await this.db;

    final DateTime now = DateTime.now();
    final currentHour = now.hour;
    final currentMinute = now.minute;

    final List<Map<String, dynamic>> maps = await db.rawQuery('''
      SELECT $idColumn, $hourColumn, $minuteColumn, $arrivalHourColumn, $arrivalMinuteColumn
      FROM $scheduleTable
      WHERE $routeIdColumn = (SELECT $idColumn FROM $routeTable WHERE $routeColumn = ?)
      AND (($hourColumn > 6 AND $hourColumn < 23) OR ($hourColumn == 23 AND $minuteColumn <= 30))
      AND (($hourColumn > ?) OR ($hourColumn = ? AND $minuteColumn > ?))
      ORDER BY $hourColumn, $minuteColumn
    ''', [route, currentHour, currentHour, currentMinute]);

    return maps.map((map) {
      return BusRepository(
        id: map[idColumn],
        route: route,
        departureHour: map[hourColumn],
        departureMinute: map[minuteColumn],
        arrivalHour: map[arrivalHourColumn],
        arrivalMinute: map[arrivalMinuteColumn],
      );
    }).toList();
  }

  Future<String> generateReturnRoute(String route) async {
    const Map<String, String> returnRoutes = {
      'Centro - Vila São Francisco': 'Vila São Francisco - Centro',
      'Vila São Francisco - Vila Operária': 'Vila Operária - Vila São Francisco',
      'Vila Operária - Vila Industrial': 'Vila Industrial - Vila Operária',
      'Vila Industrial - Jardim Panorama': 'Jardim Panorama - Vila Industrial',
      'Jardim Panorama - Vila Pioneiro': 'Vila Pioneiro - Jardim Panorama',
      'Vila Pioneiro - Vila Concórdia': 'Vila Concórdia - Vila Pioneiro',
      'Vila Concórdia - Jardim La Salle': 'Jardim La Salle - Vila Concórdia',
      'Jardim La Salle - Jardim Bressan': 'Jardim Bressan - Jardim La Salle',
      'Jardim Bressan - Vila Becker': 'Vila Becker - Jardim Bressan',
      'Vila Becker - Vila Boa Esperança': 'Vila Boa Esperança - Vila Becker',
    };

    return returnRoutes[route] ?? 'Rota de retorno não encontrada';
  }
}
